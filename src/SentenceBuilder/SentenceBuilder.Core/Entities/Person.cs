﻿using System.ComponentModel.DataAnnotations;

namespace SentenceBuilder.Core.Entities
{
    public class Person : BaseEntity
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }
    }
}
