﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SentenceBuilder.Core.Entities
{
    public class Sentence : BaseEntity
    {
        [Required]
        public int TopicId { get; set; }

        public Topic Topic { get; set; }

        [Required]
        public int SentenceNumber { get; set; }

        [MaxLength(200)]
        public string ShortDescription { get; set; }

        [MaxLength(2000)]
        public string Description { get; set; }

        public string Picture { get; set; }

        public List<Word> Words { get; set; }

        public List<AnswerWord> AnswerWords { get; set; }
    }
}
