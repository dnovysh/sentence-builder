﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SentenceBuilder.Core.Entities
{
    public class Section : BaseEntity
    {
        [Required]
        public int CourseId { get; set; }

        public Course Course { get; set; }

        [Required]
        public int DisplayOrder { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
