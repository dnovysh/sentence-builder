﻿using System.ComponentModel.DataAnnotations;

namespace SentenceBuilder.Core.Entities
{
    public class Topic : BaseEntity
    {
        [Required]
        public int SectionId { get; set; }

        public Section Section { get; set; }

        [Required]
        public int DisplayOrder { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
