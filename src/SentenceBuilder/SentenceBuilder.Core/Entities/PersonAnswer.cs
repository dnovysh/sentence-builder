﻿using System.ComponentModel.DataAnnotations;

namespace SentenceBuilder.Core.Entities
{
    public class PersonAnswer : BaseEntity
    {
        [Required]
        public int PersonId { get; set; }

        [Required]
        public int AnswerWordId { get; set; }

        [Required]
        public int WordId { get; set; }
    }
}
