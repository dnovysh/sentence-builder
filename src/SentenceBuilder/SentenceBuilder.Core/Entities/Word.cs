﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SentenceBuilder.Core.Entities
{
    public class Word : BaseEntity, IComparable<Word>
    {
        [Required]
        public int SentenceId { get; set; }

        public Sentence Sentence { get; set; }

        [Required]
        public int DisplayOrder { get; set; }

        [Required]
        public string Text { get; set; }

        public int CompareTo(Word other)
        {
            if (other is null) return -1;

            if (SentenceId != other.SentenceId)
            {
                throw new ArgumentException(
                    "Слова должны принадлежать одному предложению");
            }

            return DisplayOrder.CompareTo(other.DisplayOrder);
        }
    }
}
