﻿using System.ComponentModel.DataAnnotations;

namespace SentenceBuilder.Core.Entities
{
    public class PersonLastSentence : BaseEntity
    {
        [Required]
        public int PersonId { get; set; }

        [Required]
        public int CourseId { get; set; }

        [Required]
        public int SentenceId { get; set; }
    }
}
