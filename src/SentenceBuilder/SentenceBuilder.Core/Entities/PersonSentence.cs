﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SentenceBuilder.Core.Entities
{
    public class PersonSentence : BaseEntity
    {
        [Required]
        public int PersonId { get; set; }

        [Required]
        public int SentenceId { get; set; }

        [Required]
        public bool Completed { get; set; }

        public DateTimeOffset CompletionDate { get; set; }
    }
}
