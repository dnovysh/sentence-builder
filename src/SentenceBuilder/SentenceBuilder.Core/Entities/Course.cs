﻿using System.ComponentModel.DataAnnotations;

namespace SentenceBuilder.Core.Entities
{
    public class Course : BaseEntity
    {
        public class Section : BaseEntity
        {
            [Required]
            public string Title { get; set; }

            public string Description { get; set; }

            public string Picture { get; set; }
        }
    }
}
