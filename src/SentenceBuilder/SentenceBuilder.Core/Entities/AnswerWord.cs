﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SentenceBuilder.Core.Entities
{
    public class AnswerWord : BaseEntity, IEquatable<AnswerWord>, IComparable<AnswerWord>
    {
        [Required]
        public int SentenceId { get; set; }

        public Sentence Sentence { get; set; }

        [Required]
        public int Position { get; set; }

        [Required]
        public string Text { get; set; }

        public int CompareTo(AnswerWord other)
        {
            if (other is null) return -1;

            if (SentenceId != other.SentenceId)
            {
                throw new ArgumentException(
                    "Слова должны принадлежать одному предложению");
            }

            return Position.CompareTo(other.Position);
        }

        public override bool Equals(object obj)
        {
            return obj is AnswerWord other && Equals(other);
        }

        public bool Equals(AnswerWord other)
        {
            if (other is null) return false;

            return SentenceId == other.SentenceId &&
                   Position == other.Position &&
                   Text == other.Text;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(SentenceId, Position, Text);
        }
    }
}
