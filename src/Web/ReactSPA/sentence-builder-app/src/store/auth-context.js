import React from 'react';

const AuthContext = React.createContext({
  isAuthenticated: false,
  userId: "",
  setIsAuthenticated: (isAuthenticated, userId) => {}
});

export default AuthContext;
