import React, { useReducer } from 'react';

import AuthContext from "./auth-context";

const defaultAuthState = {
  isAuthenticated: false,
  userId: ""
};

const authReducer = (_state, action) => {
  if (action.type === "SET_IS_AUTHENTICATED") {
    return action.value;
  }

  return defaultAuthState;
}

const AuthProvider = props => {
  const [authState, dispatchAuthAction] = useReducer(authReducer, defaultAuthState);

  const setIsAuthenticatedHandler = (isAuthenticated, userId) => {
    dispatchAuthAction({
      type: "SET_IS_AUTHENTICATED",
      value: { isAuthenticated: isAuthenticated, userId: userId },
    });
  };

  const authContext = {
    isAuthenticated: authState.isAuthenticated,
    userId: authState.userId,
    setIsAuthenticated: setIsAuthenticatedHandler
  };

  return (  
    <AuthContext.Provider value={authContext}>
      {props.children}
    </AuthContext.Provider>
  );
}
 
export default AuthProvider;