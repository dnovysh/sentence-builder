import DUMMY_COURSES from "./dummy-courses";

import DefaultCoursePicture from "../assets/default-course-picture.png";

export function getCourses(searchString) {
  let courseList = [];

  if (searchString) {

    console.log('filtering');

    courseList = DUMMY_COURSES.filter((course) =>
      course.title.toLowerCase().includes(searchString.toLowerCase())
    );
  } else {
    courseList = DUMMY_COURSES;
  }

  const courseListDto = courseList.map((course) => {
    return {
      id: course.id,
      title: course.title,
      description: course.description,
      pictureUrl: course.pictureUrl ? course.pictureUrl : DefaultCoursePicture,
    };
  });

  return courseListDto;
}
