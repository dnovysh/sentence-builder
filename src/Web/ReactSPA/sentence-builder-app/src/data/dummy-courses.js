const DUMMY_COURSES =
[
  {
    id: 0,
    title: "Beginner",
    description: "beginner english course",
    pictureUrl: "",
  },
  {
    id: 1,
    title: "Beginner",
    description: "beginner english course",
    pictureUrl: "",
  },
  {
    id: 2,
    title: "Elementary",
    description: "elementary english course",
    pictureUrl: "/courseimg/img2.jpg",
  },
  {
    id: 10,
    title: "English for students",
    description: "Grammar course",
    pictureUrl: "/courseimg/logo192.png",
  },
  {
    id: 3,
    title: "Pre Intermediate",
    description: "Pre intermediate english course",
    pictureUrl: "",
  },
  {
    id: 4,
    title: "Intermediate",
    description: "Intermediate english course",
    pictureUrl: "",
  },
  {
    id: 5,
    title: "Upper Intermediate long string",
    description: "Upper intermediate english course long string",
    pictureUrl: "/courseimg/img1.png",
  },
  {
    id: 6,
    title: "Advanced",
    description: "Advanced english course",
    pictureUrl: "",
  },
];

export default DUMMY_COURSES;
