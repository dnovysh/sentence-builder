import React, { useRef, useImperativeHandle } from "react";
import TextField from '@material-ui/core/TextField';

const CourseSearchInput = React.forwardRef((props, ref) => {
  const inputRef = useRef();

  const activate = () => {
    inputRef.current.focus();
  }

  useImperativeHandle(ref, () => {
    return {
      focus: activate
    };
  });

  console.log("search input");

  return (
    <div>
      <TextField
        style={{ padding: "24px 24px 24px 40px" }}      
        inputRef={inputRef}
        id="courseSearchInput"
        value={props.value}
        onChange={props.onChange}
        placeholder="Search for Courses"
        margin="normal"
      />
    </div>
  );
});

export default React.memo(CourseSearchInput);
