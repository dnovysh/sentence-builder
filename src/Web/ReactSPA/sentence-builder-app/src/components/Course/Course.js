import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import classes from './Course.module.css';

const Course = (props) => {
  return (
    <Card className={classes.card}>
      <CardMedia
        style={{ height: 0, paddingTop: "70%" }}
        image={props.course.pictureUrl}
        title={props.course.title}
      />
      <CardContent>
        <Typography
          gutterBottom
          component="h6"
          className={classes.truncateText}
        >
          <strong>{props.course.title}</strong>
        </Typography>
        <Typography component="p" className={classes.truncateText}>
          {props.course.description}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default Course
