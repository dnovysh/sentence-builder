import React from 'react';

import Grid from '@material-ui/core/Grid';

import Course from './Course';

const CourseListGrid = (props) => {

  return (
    <Grid
      container
      direction="row"
      justify="flex-start"
      alignItems="center"
      spacing={5}
      style={{ width: "100%", paddingTop: "24px", paddingBottom: "24px" }}
    >
      {props.courses.map((course) => (
        <Grid key={"course_" + course.id} item lg={3} md={4} sm={6} xs={12}>
          <Course course={course} />
        </Grid>
      ))}
    </Grid>
  );
}

export default React.memo(CourseListGrid);
