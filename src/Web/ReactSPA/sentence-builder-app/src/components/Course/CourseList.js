import React, { useCallback, useState, useRef, useEffect } from "react";

import CourseSearchInput from "./CourseSearchInput";
import CircularIndeterminate from "../UI/CircularIndeterminate";

import classes from './CourseList.module.css';
import CourseListGrid from "./CourseListGrid";
import { getCourses } from "../../data/CourseListData";
import BackdropLocal from "../Layout/BackdropLocal";


const CourseList = () => {
  const [courses, setCourses] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [searchString, setSearchString] = useState('');

  const searchInputRef = useRef();

  useEffect(() => {
    searchInputRef.current.focus();
  }, [courses]);

  const searchInputChangeHandler = useCallback((event) => {
    if (event.target.value) {
      setSearchString(event.target.value);
    } else {
      setSearchString('');
    }
  }, []);

  useEffect(() => {
    setIsLoading(true);
    if (searchString === "") {
      setCourses(getCourses(searchString));
      setIsLoading(false);
      return;
    }
    const timer = setTimeout(() => {
      setCourses(getCourses(searchString));
      setIsLoading(false);
    }, 750);
    return () => {
      clearTimeout(timer);
    };
  }, [searchString]);

  console.log("course list");

  return (
    <>
      <CourseSearchInput ref={searchInputRef} value={searchString} onChange={searchInputChangeHandler} />
      {
        <div className={classes["list-root"]}>
          <div className={classes["list-container"]}>
            {isLoading && <BackdropLocal><CircularIndeterminate /></BackdropLocal>}
            <CourseListGrid  courses={courses} />
          </div>
        </div>
      }
    </>
  );
}

export default CourseList;
