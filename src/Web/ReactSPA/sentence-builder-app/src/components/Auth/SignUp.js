import React from 'react';
import ReactDOM from 'react-dom';
import { Link as RouterLink, useLocation } from 'react-router-dom';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Backdrop from '../Layout/Backdrop';
import logoImage from '../../assets/avatar-logo.png';
import { IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
  container: {
    position: 'fixed',
    top: theme.spacing(12),
    left: '50%',
    transform: 'translate(-50%)',
    backgroundColor: 'white',
    borderRadius: '4px',
    overflowY: 'unset',
    zIndex: '30',
  },
  paper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    position: "relative",
  },
  avatar: {
    margin: theme.spacing(3, 3, 5, 3),
    backgroundColor: "#20232a",
    width: theme.spacing(10),
    height: theme.spacing(10),
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginBottom: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(5, 0, 3),
  },
  closeButton: {
    position: "absolute",
    top: "3px",
    right: "-21px",
    [theme.breakpoints.down('xs')]: {
      right: "-13px",
    },
    borderRadius: "4px",
    backgroundColor: "white",
    color: "darkslategray",
    "&:hover": {
      backgroundColor: "lightgray",
      color: "black",
    },
  },
}));

export default function SignUp(props) {
  const classes = useStyles();
  const location = useLocation();

  const portalElement = document.getElementById("overlays");

  const prevPath = location.state.prevPath;
 
  const cancelHandler = () => {
    props.onCancel(prevPath);
  }

  return (
    <>
      {ReactDOM.createPortal(
        <Backdrop onClick={cancelHandler} />,
        portalElement
      )}
      {ReactDOM.createPortal(
        <Container component="div" maxWidth="xs" className={classes.container}>
          <CssBaseline />
          <div className={classes.paper}>
            <IconButton
              onClick={cancelHandler}
              type="button"
              color="primary"
              size="small"
              className={classes.closeButton}
            >
              <CloseIcon />
            </IconButton>
            <Avatar alt={""} className={classes.avatar} src={logoImage} />
            <form className={classes.form} noValidate>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    autoComplete="fname"
                    name="firstName"
                    variant="outlined"
                    required
                    fullWidth
                    id="signUpFirstName"
                    label="First Name"
                    autoFocus
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    variant="outlined"
                    fullWidth
                    id="signUpLastName"
                    label="Last Name"
                    name="lastName"
                    autoComplete="lname"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="signUpEmail"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="signUpPassword"
                    autoComplete="current-password"
                  />
                </Grid>
              </Grid>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                Sign Up
              </Button>
              <Grid container justify="flex-end">
                <Grid item>
                  <Link to={{pathname: '/signin', state: { prevPath: prevPath }}} variant="body2" component={RouterLink}>
                    Already have an account? Sign in
                  </Link>
                </Grid>
              </Grid>
            </form>
          </div>
        </Container>,
        portalElement
      )}
    </>
  );
}
