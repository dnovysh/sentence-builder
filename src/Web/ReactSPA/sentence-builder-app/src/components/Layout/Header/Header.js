import React, { useContext } from 'react';
import { Link, useLocation } from 'react-router-dom';

import classes from './Header.module.css';
import logoImage from '../../../assets/header-logo.png';
import AuthContext from '../../../store/auth-context';

const Header  = props => {
  const authCtx = useContext(AuthContext);
  const isAuthenticated = authCtx.isAuthenticated;
  const location = useLocation();

  return (
    <header className={classes.header}>
      <div className={classes.container}>
        <Link className={`${classes.logo} ${classes.link}`} to="/">
          <img src={logoImage} alt="" height="20" />
          <span className={classes["logo-span"]}>Sentence Builder</span>
        </Link>
        <nav className={classes.navbar}></nav>
        <div className={classes.auth}>
          {!isAuthenticated && <Link to={{pathname: '/signup', state: { prevPath: location.pathname }}}>Sign Up</Link>}
          {!isAuthenticated && <Link to={{pathname: '/signin', state: { prevPath: location.pathname }}}>Sign In</Link>}
          {isAuthenticated && <Link to="/logout">Logout</Link>}
        </div>
      </div>
    </header>
  );
};

export default Header;
