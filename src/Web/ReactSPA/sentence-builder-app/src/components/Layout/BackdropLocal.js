import classes from './BackdropLocal.module.css';

const BackdropLocal = (props) => {
  return <div className={classes.local}>{props.children}</div>
}

export default BackdropLocal;
