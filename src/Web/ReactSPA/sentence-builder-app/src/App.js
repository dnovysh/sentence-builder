import React from 'react';
import { Redirect, Route, Switch, useHistory } from 'react-router-dom';

import classes from "./App.module.css";
import SignIn from './components/Auth/SignIn';
import SignUp from './components/Auth/SignUp';
import CourseList from './components/Course/CourseList';
import CourseContent from './components/CourseContent/CourseContent';
import Footer from "./components/Layout/Footer";
import Header from "./components/Layout/Header/Header";
import AuthProvider from "./store/AuthProvider";
import NotFound from './pages/NotFound';

function App() {
  const history = useHistory();

  const signCancelHandler = (prevPath) => {
    history.push(prevPath ? prevPath : "/courses");
  }

  return (
    <AuthProvider>
      <div className={classes.app}>
        <Header />
        <main style={{ paddingTop: "60px", flexGrow: 1 }}>
          <Switch>
            <Route path="/" exact>
              <Redirect to="/courses" />
            </Route>
            <Route path="/courses" exact>
              <CourseList />
            </Route>
            <Route path="/courses/:courseId">
              <CourseContent />
            </Route>
            <Route path="/signin">
              <CourseList />
              <SignIn onCancel={signCancelHandler} />
            </Route>
            <Route path="/signup">
              <CourseList />
              <SignUp onCancel={signCancelHandler} />
            </Route>
            <Route path="*">
              <NotFound />
            </Route>
          </Switch>
        </main>
        <Footer />
      </div>
    </AuthProvider>
  );
}

export default App;
