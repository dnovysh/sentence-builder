import classes from './NotFound.module.css';

const NotFound = () => {
  return (
    <div className={classes.container}>
      <h3>Page not found!</h3>
    </div>
  );
};

export default NotFound;
